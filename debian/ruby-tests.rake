require 'gem2deb/rake/testtask'

ENV["LC_ALL"] = 'C.UTF-8'

# FIXME to support building with an older version of ruby-git installed
ENV["RUBYLIB"] ||= 'lib'
ENV["RUBYLIB"] += ':lib'

home = ENV["HOME"] = File.dirname(__FILE__) + '/home'
at_exit { FileUtils::Verbose.rm_rf(home) }
task :config do
  mkdir_p home
  sh 'git config --global user.email "git@example.com"' if `git config --global user.email`.strip.empty?
  sh 'git config --global user.name "GitExample"' if `git config --global user.name`.strip.empty?
end

Gem2Deb::Rake::TestTask.new(:test) do |t|
  t.libs = ['test']
  t.test_files = FileList['tests/**/test_*.rb']
end

task :default => [:config, :test]
